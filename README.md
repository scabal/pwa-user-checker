# BBVA User Checker

This is a simple PWA user checker

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To run the code on your machine, you will need first to install:

- Node
- npm

> The latest the version, the better!

### Installing

Now that you have installed the needed software, let's look into getting this instance to run!

Run the following from the root of the project:

```
npm install
```

This will install all the dependencies for both the client and the server.

Finally, to kick things up, run the following:

```
npm start
```

If there are no errors and a webpage appears and it's functional, then you are pretty much set!

## Running the tests

you can run the existing tests with:

```
npm run test
```

> If you run that command from the root, it will run both the client and the server tests!

## Architecture

### Client-Side

Most of the code is in the `client` folder.

The project has been scaffolded using `create-react-app` with the addition of `React Router` to do the routing between the time tracking screen and the calendar screen.

A data layer abstraction `dataStored` is used to retrieve and store data. It is currently only using `localStorage`, but could be made to point to a REST API or any other fallback as well without disrupting the UI layer.

### Server-Side

As you can see, the `server` folder is currently quite empty and just serving `index.html` in production, but can support SSR or a persistence layer in the future.

All the data is abstracted away by the `dataStored` which for now only uses `localStorage` on the `client`.

## Contributing

-- TODO --

## Authors

- **Samuel Cabal Lozano**

## License

MIT
