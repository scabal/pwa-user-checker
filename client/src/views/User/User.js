import React, {useState, useCallback, useEffect } from 'react'

import Button from '../../components/Button';
import Counter from '../../components/Counter';
import Whisper from '../../components/Whisper';
import Loading from '../../components/Loading';

import './User.css';

const User = (props) => {
    const { dataStored } = props;

    const [user, setUser] = useState({
        name: '',
        lastDate: '',
        counter: 0, // in seconds
        interval: null
      });
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(true)

    const startTimer = useCallback(() => {
      
      
      let lastDate
        try {
          let collection = localStorage.getItem('sessions');
          if(collection){
            collection = JSON.parse(collection)
            lastDate = collection.lastDate
          }else{
            lastDate = new Date(Date.now()).toLocaleString(); // firestore
          }
          // lastDate = new Date(Date.now()).toLocaleString(); // firestore
        } catch (err) {
          lastDate = new Date(Date.now()).toLocaleString(); // firestore
        } finally {
          setLoading(false)
        }
    
        const interval = setInterval(() => {
            setUser(prev => ({ ...prev, counter: prev.counter + 1 }))
        }, 1000 /* 1 second */);
        
        setUser(prev => ({ ...prev, lastDate, interval }))
    },[dataStored],);

    const stopTimer = useCallback(() => {
        
        const { interval, name } = user;

        const stop = new Date(Date.now()).toLocaleString();

        clearInterval(interval);

        dataStored
          .put('sessions', { name: name || 'No Name', lastDate:stop, stop })
          .catch(err => {
            setError(true)
          });
        //firestore

        setUser(prev => ({ ...prev, counter: 0, lastDate: '', name: '' }));
    },[user],)
    

    useEffect(() => {
        window.scrollTo(0, 0);
        startTimer()
    },[]);

    return loading ? (
      <Loading />
    ) : (
        <div className="user-checker">
            <Whisper
              display={error}
              type="error"
              text="Oops, seems like something went wrong ... Please try again!"
            />
            <div className="user-checker-name">
              <h1 className="user-checker-title">Welcome!</h1>
              <h5 className="user-checker-sub">The last time you accessed was</h5>
            </div>
            <div className="user-checker-counter">
              <Counter  value={user.lastDate} />
            </div>
            <div className="user-checker-button">
                <Button className="user-checker-stop" onClick={() => stopTimer()}>
                    LOGOUT
                </Button>
            </div>
        </div>
    )
}

export default User
