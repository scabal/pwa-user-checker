import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import dataStored from './dataStored';

import registerServiceWorker from './utils/register-service-worker';

dataStored.init();

ReactDOM.render(<App dataStored={dataStored} />, document.getElementById('root'));

registerServiceWorker();
