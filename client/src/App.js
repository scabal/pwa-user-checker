import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';

import User from './views/User';

import './App.css';


function App(props) {
    const { dataStored } = props;

    return (
      <Router>
        <div className="app">
          <Header>BBVA User Checker</Header>
          <Main>
            <Route
              path="/"
              dataStored={dataStored}
              render={prev => <User {...prev} dataStored={dataStored} />}
            />
          </Main>
          <Footer>
            Made by Samuel Cabal Lozano
          </Footer>
        </div>
      </Router>
    );

}

export default App;
