import React from "react";
import moment from "moment";

import './index.css';

const formatNumber = number => {
  return (number < 10 ? '0'+ number : number);
}

const formatCounter = finishDate => {
  if (typeof finishDate !== "string") {
    // throw new TypeError('`finishDate` needs to be a String.');
    finishDate = new Date(Date.now()).toLocaleString();
  }
  
  let lastDate = moment(finishDate, 'DD/MM/YYYY HH:mm:ss');
  const today = moment(new Date());
  
  let duration = moment.duration(today.diff(lastDate));
  let days = formatNumber(duration.days());
  let hours = formatNumber(duration.hours());
  let minutes = formatNumber(duration.minutes());
  let seconds = formatNumber(duration.seconds());

  return (
    <div className="container-counter">
      <div className="sty-row-counter">
        <div className="column-counter">
          <div className="counter-number">{days}</div>
          <div className="counter-text">Days</div>
        </div>
        <div className="column-counter">
          <div className="counter-number">{hours}</div>
          <div className="counter-text">Hours</div>
        </div>
        <div className="column-counter">
          <div className="counter-number">{minutes}</div>
          <div className="counter-text">Minutes</div>
        </div>
        <div className="column-counter">
          <div className="counter-number">{seconds}</div>
          <div className="counter-text">Seconds</div>
        </div>
      </div>
    </div>
  );
};

export default formatCounter;
